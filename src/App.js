import { useState } from 'react';
import axios from 'axios';
import './App.css';

function App() {

  const [num1, setNum1] = useState(0);
  const [num2, setNum2] = useState(0);
  const [operator, setOperator] = useState('')

  function numberClick(i) {
    setNum1(num1 * 10 + i);
  }

  function clickOperator(c) {
    setNum2(num1);
    setNum1(0);
    setOperator(c);
  }

  function getResult() {
    let queryUrl = '';
    switch (operator) {
      case '+':
        queryUrl = 'https://7leu5b4x2b.execute-api.ap-southeast-1.amazonaws.com/dev/arithmetic/add';
        break;
      case '-':
        queryUrl = 'https://7leu5b4x2b.execute-api.ap-southeast-1.amazonaws.com/dev/arithmetic/subtract';
        break;
      case '*':
        queryUrl = 'https://7leu5b4x2b.execute-api.ap-southeast-1.amazonaws.com/dev/arithmetic/multiply';
        break;
      case '/':
        queryUrl = 'https://7leu5b4x2b.execute-api.ap-southeast-1.amazonaws.com/dev/arithmetic/divide';
        break;
    }

    if (queryUrl.length > 0) {
      try {
        axios.post(queryUrl, {
          n1: num2,
          n2: num1,
        }).then(res => {
          setNum1(res.data);
          setNum2(0);
          setOperator('');
        })
      } catch (e) {
        console.log(e)
      }
    }
  }

  function clear() {
    setNum1(0);
    setNum2(0);
    setOperator('');
  }


  return (
    <div className="App">
      <h3 className="">
        {num2}
      </h3>
      <h3> {operator}</h3>
      <h1 className="">
        {num1}
      </h1>
      <div>
        <button className="" onClick={() => { numberClick(7) }}> 7 </button>
        <button className="" onClick={() => { numberClick(8) }}> 8 </button>
        <button className="" onClick={() => { numberClick(9) }}> 9 </button>
      </div>
      <div>
        <button className="" onClick={() => { numberClick(4) }}> 4 </button>
        <button className="" onClick={() => { numberClick(5) }}> 5 </button>
        <button className="" onClick={() => { numberClick(6) }}> 6 </button>
      </div>
      <div>
        <button className="" onClick={() => { numberClick(1) }}> 1 </button>
        <button className="" onClick={() => { numberClick(2) }}> 2 </button>
        <button className="" onClick={() => { numberClick(3) }}> 3 </button>
      </div>
      <div>
        <button className="" onClick={() => { numberClick(0) }}> 0 </button>
      </div>
      <div>
        <button className="" onClick={() => { clickOperator('+') }}> + </button>
        <button className="" onClick={() => { clickOperator('-') }}> - </button>
        <button className="" onClick={() => { clickOperator('*') }}> * </button>
        <button className="" onClick={() => { clickOperator('/') }}> / </button>
      </div>
      <div>
        <button className="" onClick={() => { getResult() }}> = </button>
        <button className="" onClick={() => { clear() }}> C </button>
      </div>
    </div>
  );
}

export default App;
