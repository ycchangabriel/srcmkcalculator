# A Useless Calculator

you are tasked to create a calculator using these API
- `POST https://7leu5b4x2b.execute-api.ap-southeast-1.amazonaws.com/dev/arithmetic/add`
- `POST https://7leu5b4x2b.execute-api.ap-southeast-1.amazonaws.com/dev/arithmetic/subtract`
- `POST https://7leu5b4x2b.execute-api.ap-southeast-1.amazonaws.com/dev/arithmetic/multiply`
- `POST https://7leu5b4x2b.execute-api.ap-southeast-1.amazonaws.com/dev/arithmetic/divide`
with the following request BODY
```
{
  "n1": 86,
  "n2": 64
}
```
\* the number 86 and 64 are served as examples. They are expected to be integers.